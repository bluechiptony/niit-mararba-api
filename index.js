const bodyParser = require("body-parser");
const express = require("express");
const niit = express();
const cors = require("cors");
const port = process.env.PORT || 7000;
const www = process.env.WWW || "./";

const applicant = require("./src/routes/applicants");

niit.disable("etag").disable("x-powered-by");
niit.use(express.static(www));
niit.use(cors());
niit.use(bodyParser.urlencoded({ extended: false }));
niit.use(bodyParser.json());

niit.use((req, res, next) => {
  next();
});

niit.use("/", applicant);
niit.get("/", (req, res) => {
  res.sendFile(`index.html`, { root: www });
});
niit.get("/api/docs", (req, res) => {
  res.sendFile(`/documentation/index.html`, { root: www });
});

niit.listen(port, () => {
  console.log(`NIIT, Listening on port ${port}`);
});
