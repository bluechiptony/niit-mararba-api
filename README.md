# Niit Mararba API

This project was generated with the bluechiptony gen-node-api script version 1.0.0.

## How to run

### Prerequisites

Postgresql/Mysql
Node JS
NPM

After cloning this repository, navigate to the folder on your local machine
run `npm install` to install required dependencies

navigate to src/config/databaseconnection.js to edit database connections to suit those of your local machine

run npm start to server the project on your local machine

the app will be available on http://localhost:7000

## Documentation

run `npm run` gen-doc to generate the app's documentation on your local machine
the documentation will be available on `path-to-project/documentation/index.html`
