const UIDGenerator = require("uid-generator");
const uniqid = require("uniqid");
const keySanitizer = require("keys-to-camelcase");

/**
 * Generates a random uniquie token with base 36 and a specified length
 */
var getToken = length => {
  const gen = new UIDGenerator(UIDGenerator.BASE36, length);
  return gen.generateSync();
};

/**
 * Generates unique code
 */
getUniqueId = uppercase => {
  if (uppercase) {
    return uniqid().toUpperCase();
  } else {
    return uniqid();
  }
};

/**
 * Generates unique code with prefix
 */
getUniqueIdWithPrefix = (prefix, uppercase) => {
  if (uppercase) {
    return uniqid(prefix).toUpperCase();
  } else {
    return uniqid(prefix);
  }
};

errorResponse = message => {
  return {
    error: message
  };
};

messageResponse = message => {
  return {
    message: message
  };
};

isInArray = (value, array) => {
  if (Array.isArray(array)) {
    return array.includes(value);
  } else {
    return false;
  }
};

module.exports = {
  keySanitizer: keySanitizer,
  getToken: getToken,
  getUniqueId: getUniqueId,
  getUniqueIdWithPrefix: getUniqueIdWithPrefix,
  messageResponse: messageResponse,
  errorResponse: errorResponse,
  isInArray: isInArray
};
