var accountTypes = {
  SUPER_ADMINISTRATOR: "SUPER ADMINISTRATOR",
  SUPPORT: "SUPPORT",
  REPORTER: "REPORTER",
  ADMINISTRATOR: "ADMINISTRATOR",
  USER: "USER"
};

module.exports = {
  accountTypes: accountTypes,
  websiteUrlActivateAccount: "http://localhost:3500/",
  websiteUrlReset: "http://localhost:3500/"
};
