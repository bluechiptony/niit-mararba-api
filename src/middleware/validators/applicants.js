const database = require("../../config/databaseconnection");
const utils = require("../../utilities/helpers");

checkIfEmailregistered = email => {
  return database("applicants")
    .select("*")
    .where({ email_address: email });
};

checkIfPhoneNumberExists = phoneNumber => {
  return database("applicants")
    .select("*")
    .where({ phone_number: phoneNumber });
};

validateApplicantPayload = async (req, res, next) => {
  let applicant = req.body;

  if (applicant !== null || applicant !== undefined) {
    if (
      applicant.emailAddress !== null ||
      applicant.emailAddress !== undefined
    ) {
      try {
        let checkEmail = await checkIfEmailregistered(applicant.emailAddress);
        if (checkEmail.length > 0) {
          res
            .status(500)
            .json(utils.errorResponse("Sorry, this email has been registered"));
          res.end();
        } else {
          next();
        }
      } catch (error) {
        console.log(error);

        res
          .status(400)
          .json(
            utils.errorResponse("Sorry, Please provide your email address")
          );
        res.end();
      }
    } else {
      res
        .status(400)
        .json(utils.errorResponse("Sorry, cannot validate your email address"));
      res.end();
    }
  } else {
    res.status(400).json(utils.errorResponse("No applicant data provided"));
    res.end();
  }
};

module.exports = {
  validateApplicantPayload: validateApplicantPayload
};
