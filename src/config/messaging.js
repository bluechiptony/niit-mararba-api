const mailConstants = {
  mailgunKey: "key-23a9499531551c899c2e5abe5445a2e9",
  mailGunDomain: "outmail.tonyegwu.com"
};

const mailgun = require("mailgun-js")({
  apiKey: mailConstants.mailgunKey,
  domain: mailConstants.mailGunDomain
});

module.exports = mailgun;
