var pg = require("pg");
pg.types.setTypeParser(20, "text", parseInt);

const host = "127.0.0.1";
const user = "tonyegwu";
const password = "";
const databaseName = "niit_mararba";
const mysqlVersion = "5.7";

//database client use "pg" for postgres and "mysql" for mysql
const client = "mysql";

const database = require("knex")({
  client: client,
  connection: {
    host: host,
    user: user,
    password: password,
    database: databaseName
  }
});

// const database = require('knex')({
//   client: 'mysql',
//   version: '5.7',
//   connection: {
//     host : '127.0.0.1',
//     user : 'root',
//     password : 'password',
//     database : 'niit_mararba'
//   }
// });

// const database = require("knex")({
//   client: "pg",
//   connection: {
//     host: "54.86.119.11",
//     user: "postgresdev",
//     password: "developer",
//     database: "goposutilities"
//   }
// });

module.exports = database;
