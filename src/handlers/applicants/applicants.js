const database = require("../../config/databaseconnection");
const utils = require("../../utilities/helpers");
const keySanitizer = utils.keySanitizer;

createApplicant = (req, res) => {
  console.log("creating applicant");

  let applicant = req.body;
  let applicantCode = utils.getToken(5);

  database("applicants")
    .insert({
      first_name: applicant.firstName,
      last_name: applicant.lastName,
      applicant_code: applicantCode,
      email_address: applicant.emailAddress,
      phone_number: applicant.phoneNumber,
      source_of_knowledge: applicant.source,
      time_slot: applicant.timeSlot,
      current_status: applicant.status
    })
    // .returning("applicant_code")
    .then(data => {
      console.log(data);

      // let applicantCode = data[0];
      res.status(201).json({
        // applicantCode: applicantCode,
        message: "Your application has been received successfully."
      });
      res.end();
    })
    .catch(error => {
      console.log(error);

      res
        .status(500)
        .json(
          utils.errorResponse(
            "Sorry Something went wrong during your registration"
          )
        );
      res.end();
    });
};

module.exports = {
  createApplicant: createApplicant
};
