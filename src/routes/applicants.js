const applicant = require("express").Router();

const applicantValidator = require("../middleware/validators/applicants");
// const tokenValidator = require("../middleware/authentication/tokenhandler");
const applicantsHandler = require("../handlers/applicants/applicants");

applicant.post(
  "/applicant/create",
  applicantValidator.validateApplicantPayload,
  applicantsHandler.createApplicant
);

module.exports = applicant;
