/**
 * @apiGroup Applicants
 * @apiName Create an applicant
 * @apiDescription Creates a new applicant
 * @api {POST} /applicant/create Create a new applicant
 * @apiParam {String} firstName applicant's first Name.
 * @apiParam {String} lastName applicant's  last Name.
 * @apiParam {String} emailAddress applicant's Email address.
 * @apiParam {String} phoneNumber applicant's phone number.
 * @apiParam {String}  applicant's exam time slot.
 * @apiParam {String}  applicant's education status.
 * @apiParamExample {json} Creat Aapplicant Example:
 * {
 *                  "firstName":"Jane",
 *                  "lastName":"Doe",
 *                  "emailAddress":"email@email.com",
 *                  "phoneNumber":"080233423323",
 *                  "sourceOfKnowledge":"Facebook",
 *                  "timeSlot":"Kester Investments, 123, bester kingdom",
 *                  "status":"Undergraduate",
 *              }
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "message": "applicant Successfully created"
 *     }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "applicant could not be created"
 *     }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "error": "Email address already exists for another applicant"
 *     }
 *
 */
